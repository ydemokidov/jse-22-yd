package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.AbstractUserOwnedEntity;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<E extends AbstractUserOwnedEntity> extends IRepository<E> {

    E add(String userId, E entity);

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator comparator);

    E findOneById(String userId, String id);

    E findOneByIndex(String userId, Integer index);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    int getSize(String userId);

}
