package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}
