package com.t1.yd.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String taskId, String projectId);

    void removeProjectById(String userId, String projectId);

    void unbindTaskFromProject(String userId, String taskId, String projectId);

}
