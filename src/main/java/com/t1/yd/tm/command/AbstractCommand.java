package com.t1.yd.tm.command;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getName();

    public abstract String getArgument();

    @Override
    public abstract Role[] getRoles();

    public abstract String getDescription();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();

        String result = "";

        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }
}
