package com.t1.yd.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project_clear";
    public static final String DESCRIPTION = "Clear project";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
