package com.t1.yd.tm.command.project;

import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project_list";
    public static final String DESCRIPTION = "Show list of projects";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);

        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
