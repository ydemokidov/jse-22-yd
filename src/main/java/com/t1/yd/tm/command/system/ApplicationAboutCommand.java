package com.t1.yd.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";
    public static final String ARGUMENT = "-a";
    public static final String DESCRIPTION = "Show info about author";


    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Author: Yuriy Demokidov");
        System.out.println("Email: ydemokidov@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
