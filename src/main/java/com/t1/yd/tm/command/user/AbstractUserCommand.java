package com.t1.yd.tm.command.user;

import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(final User user) {
        if (user == null) return;
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
    }

}
