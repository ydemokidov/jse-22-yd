package com.t1.yd.tm.command.user;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    private final String name = "user_lock";

    private final String description = "User lock";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        getUserService().lockByLogin(login);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getDescription() {
        return description;
    }
}
