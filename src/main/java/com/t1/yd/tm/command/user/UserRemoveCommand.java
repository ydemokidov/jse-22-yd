package com.t1.yd.tm.command.user;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    private final String name = "user_remove";

    private final String description = "Remove user";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        final User removedUser = getUserService().removeByLogin(login);
        System.out.println("[USER " + removedUser.getLogin() + " REMOVED]");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getDescription() {
        return description;
    }

}
