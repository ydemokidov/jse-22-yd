package com.t1.yd.tm.command.user;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    private final String name = "user_unlock";

    private final String description = "User unlock";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockByLogin(login);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public String getDescription() {
        return description;
    }

}
