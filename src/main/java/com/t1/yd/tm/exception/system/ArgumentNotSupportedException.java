package com.t1.yd.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public ArgumentNotSupportedException(String argument) {
        super("Error! Argument " + argument + " is not supported..");
    }
}
