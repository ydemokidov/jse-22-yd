package com.t1.yd.tm.exception.user;

import com.t1.yd.tm.exception.AbstractException;

public class AbstractUserException extends AbstractException {

    public AbstractUserException() {
    }

    public AbstractUserException(String message) {
        super(message);
    }

    public AbstractUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(Throwable cause) {
        super(cause);
    }

    public AbstractUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
