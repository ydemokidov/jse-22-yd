package com.t1.yd.tm.exception.user;

public class IsEmailExistException extends AbstractUserException {

    public IsEmailExistException() {
        super("Error! Email exists...");
    }
}
