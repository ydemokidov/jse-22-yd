package com.t1.yd.tm.model;

public abstract class AbstractUserOwnedEntity extends AbstractEntity {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
