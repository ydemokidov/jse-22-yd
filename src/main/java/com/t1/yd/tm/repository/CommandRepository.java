package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.exception.field.NameEmptyException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commandsByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commandsByName = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) throw new NullPointerException("Error! Command is null...");
        final String name = command.getName();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        commandsByName.put(name, command);
        final String argument = command.getArgument();
        if (argument == null || argument.isEmpty()) return;
        commandsByArgument.put(argument, command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        return commandsByArgument.get(argument);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commandsByName.get(name);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandsByName.values();
    }

}
