package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return entities.stream()
                .filter(e -> (projectId.equals(e.getProjectId()) && userId.equals(e.getUserId())))
                .collect(Collectors.toList());
    }
}
