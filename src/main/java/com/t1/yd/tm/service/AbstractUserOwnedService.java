package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.api.service.IUserOwnedService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;

import java.util.Comparator;
import java.util.List;

public class AbstractUserOwnedService<E extends AbstractUserOwnedEntity, R extends IUserOwnedRepository<E>> extends AbstractService<E, R> implements IUserOwnedService<E> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public E add(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (entity == null) throw new NullPointerException("Error! Entity is null...");
        return repository.add(userId, entity);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId, comparator);
    }

    public List<E> findAll(final String userId, final Sort sort) {
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public E findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<E> userEntities = findAll(userId);
        if (index < 0 || index >= userEntities.size()) throw new IndexIncorrectException();

        return repository.findOneByIndex(userId, index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<E> userEntities = findAll(userId);
        if (index < 0 || index >= userEntities.size()) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public int getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return findAll(userId).size();
    }

}
